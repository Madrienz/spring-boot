package org.madrien.exceptions;

public class NoEntityForDeletionException extends RuntimeException {

    public NoEntityForDeletionException() {
        super("The entity does not exist.");
    }
}
