package org.madrien.exceptions.handler;

import org.madrien.exceptions.BookNotFoundException;
import org.madrien.exceptions.NoEntityForDeletionException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class BookExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(BookNotFoundException.class)
    public void handleBookNotFound(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value());
    }

    @ExceptionHandler(NoEntityForDeletionException.class)
    public void handleNoEntityForDeletion(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NO_CONTENT.value());
    }
}
