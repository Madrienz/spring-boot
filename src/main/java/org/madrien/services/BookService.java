package org.madrien.services;

import org.madrien.dao.BookRepository;
import org.madrien.entities.Book;
import org.madrien.exceptions.BookNotFoundException;
import org.madrien.exceptions.NoEntityForDeletionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public List<Book> findAllBooks() {
        return bookRepository.findAll();
    }

    public Book findBookById(Long id) {
        Optional<Book> opt = bookRepository.findById(id);
        return opt.orElseThrow(BookNotFoundException::new);
    }

    public Book findBookByTitle(String title) {
        String transformed = title.replace('_', ' ');
        Optional<Book> opt = bookRepository.findByTitleIgnoreCase(transformed);

        return opt.orElseThrow(BookNotFoundException::new);
    }

    public List<Book> findByAuthorLastName(String lastName) {
        List<Book> list = bookRepository.findByAuthorLastName(lastName);
        if(list.isEmpty()) {
            throw new BookNotFoundException();
        }
        return list;
    }

    public Book createBook(@Valid Book book, BindingResult result) throws BindException {
        if(result.hasErrors()) {
            throw new BindException(result);
        }
        return bookRepository.save(book);
    }

    public Book updateBook(@Valid Book book, BindingResult result) throws BindException {
        if(result.hasErrors()) {
            throw new BindException(result);
        }
        if(!bookRepository.findById(book.getId()).isPresent()) {
            throw new BookNotFoundException();
        }
        return bookRepository.save(book);
    }

    public void deleteBookById(Long id) {
        if(!bookRepository.findById(id).isPresent()) {
            throw new NoEntityForDeletionException();
        }
        bookRepository.deleteById(id);
    }
}
