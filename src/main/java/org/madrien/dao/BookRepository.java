package org.madrien.dao;

import org.madrien.entities.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Long> {

    Optional<Book> findByTitleIgnoreCase(String lastName);

    @Query("SELECT b FROM Book b WHERE LOWER(b.author.lastName) = LOWER(:lastName)")
    List<Book> findByAuthorLastName(@Param("lastName") String lastName);

}
