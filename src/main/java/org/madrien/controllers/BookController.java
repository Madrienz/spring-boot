package org.madrien.controllers;

import org.madrien.entities.Book;
import org.madrien.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping
    public List<Book> getAllBooks() {
        return bookService.findAllBooks();
    }

    @GetMapping("/{id}")
    public Book getBookById(@PathVariable("id") Long id) {
        return bookService.findBookById(id);
    }

    @GetMapping("/titles/{title}")
    public Book getBooksByTitle(@PathVariable("title") String title) {
        return bookService.findBookByTitle(title);
    }

    @GetMapping("/authors/{lastName}")
    public List<Book> getByAuthorLastName(@PathVariable("lastName") String lastName) {
        return bookService.findByAuthorLastName(lastName);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Book createBook(@RequestBody Book book, BindingResult result) throws BindException {
        return bookService.createBook(book, result);
    }

    @PutMapping
    public Book updateBook(@RequestBody Book book, BindingResult result) throws BindException {
        return bookService.updateBook(book, result);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteBookById(@PathVariable("id") Long id) {
        bookService.deleteBookById(id);
    }
}
