INSERT INTO publishers
    VALUES (DEFAULT, 'Grant'),
           (DEFAULT, 'Faber and Faber'),
           (DEFAULT, 'Crown'),
           (DEFAULT, 'Scribner'),
           (DEFAULT, 'Random House');

INSERT INTO authors
    VALUES (DEFAULT, 'William', 'Golding'),
            (DEFAULT, 'Stephen', 'King'),
            (DEFAULT, 'Andy', 'Weir'),
            (DEFAULT, 'Ernest', 'Cline');

INSERT INTO books
    VALUES (DEFAULT, 1954, 'Lord of the Flies', 1, 2),
            (DEFAULT, 2014, 'The Martian', 3, 3),
            (DEFAULT, 1982, 'The Dark Tower The Gunslinger', 2, 1),
            (DEFAULT, 2011, 'Ready Player One', 4, 5),
            (DEFAULT, 2001, 'Dreamcatcher', 2, 4);