package org.madrien;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.madrien.controllers.BookControllerRestTemplateTest;
import org.madrien.controllers.BookControllerSpringBootTest;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        BookControllerSpringBootTest.class,
        BookControllerRestTemplateTest.class
})

public class TestSuite {
}
