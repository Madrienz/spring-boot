package org.madrien;

import org.madrien.entities.Book;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application-test.properties")
public class TestObjectsConfig {

    @Bean
    @ConfigurationProperties(prefix = "test")
    public Book newBook() {
        return new Book();
    }

}
