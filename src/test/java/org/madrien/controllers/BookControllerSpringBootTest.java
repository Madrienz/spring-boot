package org.madrien.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.madrien.SpringBootHomeworkApplication;
import org.madrien.entities.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment =
        SpringBootTest.WebEnvironment.MOCK,
        classes = SpringBootHomeworkApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("local")
public class BookControllerSpringBootTest {

    private static final int EXISTING_ID = 2;
    private static final String EXISTING_TITLE = "Lord of the Flies";
    private static final String NON_EXISTING_TITLE = "Lord of the Fries";
    private static final String EXISTING_AUTHOR_LAST_NAME = "Weir";
    private static final String NON_EXISTING_AUTHOR_LAST_NAME = "Malcovic";

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private Book newBook;

    @Test
    public void getBooksByTitle() throws Exception {
        mockMvc.perform(get("/books/titles/" + EXISTING_TITLE))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is(EXISTING_TITLE)));
    }

    @Test
    public void getByAuthorLastName() throws Exception {
        mockMvc.perform(get("/books/authors/" + EXISTING_AUTHOR_LAST_NAME))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].author.lastName", is(EXISTING_AUTHOR_LAST_NAME)));
    }

    @Test
    public void createBook() throws Exception {
        mockMvc.perform(post("/books")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(newBook)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.title", is(newBook.getTitle())));
    }

    @Test
    @DirtiesContext
    public void updateBook() throws Exception {
        newBook.setId(Integer.valueOf(EXISTING_ID).longValue());
        mockMvc.perform(put("/books")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(newBook)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(newBook.getId().intValue())))
                .andExpect(jsonPath("$.title", is(newBook.getTitle())));
    }

    @Test
    public void bookNotFoundByTitle() throws Exception {
        mockMvc.perform(get("/books/titles/" + NON_EXISTING_TITLE))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void bookNotFoundByAuthor() throws Exception {
        mockMvc.perform(get("/books/authors/" + NON_EXISTING_AUTHOR_LAST_NAME))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void bookNotUpdated() throws Exception {
        mockMvc.perform(put("/books")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(newBook)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}