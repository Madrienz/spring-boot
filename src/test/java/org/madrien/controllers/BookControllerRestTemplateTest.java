package org.madrien.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.madrien.SpringBootHomeworkApplication;
import org.madrien.entities.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment =
        SpringBootTest.WebEnvironment.DEFINED_PORT,
        classes = SpringBootHomeworkApplication.class)
@ActiveProfiles("local")
public class BookControllerRestTemplateTest {

    private static final int EXISTING_ID = 2;
    private static final int NON_EXISTING_ID = 12;

    String resourceUrl = "/books";

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void getAllBooks() {
        ResponseEntity<List<Book>> response = testRestTemplate.exchange(resourceUrl,
                                                                        HttpMethod.GET,
                                                                        null,
                                                                        new ParameterizedTypeReference<List<Book>>() {});
        List<Book> list = response.getBody();
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(list, hasSize(5));
        assertThat(list.get(1).getTitle(), equalTo("The Martian"));
    }

    @Test
    public void getBookById() {
        ResponseEntity<Book> response = testRestTemplate.getForEntity(resourceUrl + "/" + EXISTING_ID, Book.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody().getId(), equalTo(Integer.valueOf(EXISTING_ID).longValue()));
    }

    @Test
    public void BookNotFoundById() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(resourceUrl + "/" + NON_EXISTING_ID, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
    }

    @Test
    public void deleteBookById() {
        ResponseEntity<String> response = testRestTemplate.exchange(resourceUrl + "/" + EXISTING_ID,
                                                                    HttpMethod.DELETE,
                                                                    null,
                                                                    String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.NO_CONTENT));
        assertThat(response.hasBody(), equalTo(false));
    }

    @Test
    public void bookNotDeleted() {
        ResponseEntity<String> response = testRestTemplate.exchange(resourceUrl + "/" + NON_EXISTING_ID,
                                                                    HttpMethod.DELETE,
                                                                    null,
                                                                    String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.NO_CONTENT));
        assertThat(response.hasBody(), equalTo(false));
    }
}