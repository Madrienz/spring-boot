# Spring Boot
Для настройки подключений к базам данных использовать:
* application-prod.properties для PostgreSQL
* application-local.properties для H2.

Для работы с PostgreSQL нужна база данных с названием books.
Профиль меняется в application.properties

REST endpoints:

* GET http://localhost:1111/books                         получить список книг.
* GET http://localhost:1111/books/{id}                    получить книгу по id.
* GET http://localhost:1111/books/titles/{title}          получить книгу по названию.
* GET http://localhost:1111/books/authors/{lastName}      получить книгу по фамилии автора.
* PUT http://localhost:1111/books                         изменить книгу.
* DELETE http://localhost:1111/books/{id}                 удалить книгу по id.
* POST http://localhost:1111/books                        создать книгу.

* http://localhost:1111/actuator/health         для просмотра статуса программы.

Также имеется Postman-коллекция с необходимыми запросами.